import React, {useContext} from 'react';
import { View, Text, StyleSheet,  FlatList,  TextInput, TouchableOpacity} from 'react-native';
import { RootContext } from './provider';

const Consumer = () => {
  
  const state = useContext(RootContext)

  const renderItem = ({item, index}) => {
    return(
      <View key={index} style={styles.name}>
        <Text style={styles.nameText}>{item.name}</Text>
        <Text style={styles.nameText}>{item.position}</Text>
      </View>
    )
  }
  return (
    <View style={styles.container}>
      <FlatList
        data={state.name}
        renderItem={renderItem}
        keyExtractor={(item, index) => index.toString()} 
        />
    </View>
  ) 
}


const styles= StyleSheet.create({
  container: {
    flex: 1,
    padding: 10
  },
  name: {
    position: 'relative',
    padding: 15,
    borderWidth: 5,
    borderColor: '#b5b2b0',
    margin: 5
  },
  nameText: {
    fontSize: 12
  }
})


export default Consumer;